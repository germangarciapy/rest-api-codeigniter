<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}

	public function getUsers(){
		return $this->db->get_where('users', array('enum_state' => 'ACTIVO'))->result_array();
	}

	public function getUser($id){
		return $this->db->get_where('users', array('id' => $id))->row_array();
	}

	public function saveUser($user){
	    try {
	        $result = $this->db->insert("users", $user);

	        $db_error = $this->db->error();
	        if (!$result) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	        }
	        return TRUE;
	    } catch (Exception $e) {
	        error_log($e->getMessage(),0);
	        return FALSE;
	    }
	}

	public function updateUser($id, $user){
		try {
	        $this->db->where("id", $id);
			$result = $this->db->update("users", $user);

	        $db_error = $this->db->error();
	        if (!$result) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	        }
	        return TRUE;
	    } catch (Exception $e) {
	        error_log($e->getMessage(),0);
	        return FALSE;
	    }
	}

	public function replaceUser($id, $user){
		try {
	        $this->db->trans_start();
	        $this->db->delete("users", array("id" => $id));
			$this->db->replace("users", $user);
	        $result = $this->db->trans_complete();

	        $db_error = $this->db->error();
	        if (!$result) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	        }
	        return TRUE;
	    } catch (Exception $e) {
	        error_log($e->getMessage(),0);
	        return FALSE;
	    }
	}

	public function deleteUser($id){
		return $this->db->delete("users", array("id" => $id));
	}

}
?>