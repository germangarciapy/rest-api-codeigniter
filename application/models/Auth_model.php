<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}

	public function login($mail,$password)
    {
        $user = $this->db->select('password,id')->from('users')->where('mail',$mail)->get()->row_array();
        if($user == NULL){
            return array('message' => 'invalid_user');	// User not found.
        } else {
            if (sha1(md5($password)) == $user['password']) {
               return array('message' => 'success', 'userId' => $user['id']); // Success
            } else {
               return array('message' => 'invalid_password');	// Wrong password
            }
        }
    }

}
?>