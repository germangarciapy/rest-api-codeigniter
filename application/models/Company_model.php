<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}

	public function getCompanies(){
		return $this->db->get_where('companies', array('enum_state' => 'ACTIVO'))->result_array();
	}

	public function getCompany($id){
		return $this->db->get_where('companies', array('id' => $id, 'enum_state' => 'ACTIVO'))->row_array();
	}

	public function saveCompany($company){
		try {
	        $result = $this->db->insert("companies", $company);

	        $db_error = $this->db->error();
	        if (!$result) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	        }
	        return TRUE;
	    } catch (Exception $e) {
	        error_log($e->getMessage(),0);
	        return FALSE;
	    }
	}

	public function updateCompany($id, $company){
		try {
	        $this->db->where("id", $id);
			$result = $this->db->update("companies", $company);

	        $db_error = $this->db->error();
	        if (!$result) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	        }
	        return TRUE;
	    } catch (Exception $e) {
	        error_log($e->getMessage(),0);
	        return FALSE;
	    }
	}

	public function replaceCompany($id, $company){
		try {
	        $this->db->trans_start();
	        $this->db->delete("companies", array("id" => $id));
			$this->db->replace("companies", $company);
	        $result = $this->db->trans_complete();

	        $db_error = $this->db->error();
	        if (!$result) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	        }
	        return TRUE;
	    } catch (Exception $e) {
	        error_log($e->getMessage(),0);
	        return FALSE;
	    }
	}

	public function deleteCompany($id){
		return $this->db->delete("companies", array("id" => $id));
	}

}
?>