<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'vendor/autoload.php'); 
use \Firebase\JWT\JWT;

/**
* Rest class
* Use for rest functions
*/
class Rest 
{
	/**
	 * Constructor function
	 * */
	function __construct(){
		date_default_timezone_set('America/Asuncion');
	}
	
	/**
	 * Check the headers
	 * */
	function checkHeaders($method = null, $contentType = null){
		//	CHECK HTTP METHOD
		$request_method = $_SERVER['REQUEST_METHOD'];
		if($request_method != $method){
			return array('status' => 400,'message' => 'Bad request.');
		}

		//	CHECK HTTP CONTENT TYPE
		$request_content_type = $_SERVER['CONTENT_TYPE'];
		if($contentType !== null){
			if($request_content_type != $contentType){
				return array('status' => 400,'message' => 'Bad request.');
			}
		}

		return array('status' => 200);
	}

	/**
	 * Check token
	 * */
	function checkToken(){
		//	CHECK TOKEN
		$payload = null;
		$request_token = $this->getBearerToken();
		if(is_null($request_token)){
			return array('status' => 401,'message' => 'Unauthorize.');
		}

		try {
			$payload = JWT::decode($request_token, SECRET_KEY, array('HS256'));
		} catch (Exception $e) {
			$message = $e->getMessage();
			return array('status' => 401,'message' => $message);
		}

		return array('status' => 200, 'payload' => $payload);
	}

	/**
	 * Check user
	 * */
	function checkUser($payload){
		//	CHECK IF USER EXISTS
		$ci =& get_instance();
		$ci->load->model('User_model');
		try {
			$user = $ci->User_model->getUser($payload->userId);
			if(empty($user)){
				return array('status' => 401,'message' => 'User not found.');
			}

			//	CHECK IF USER IS ACTIVE
			if($user['enum_state'] != "ACTIVO"){
				return array('status' => 401,'message' => 'User not active.');
			}	
		} catch (Exception $e) {
			error_log($e->getMessage(),0);
			return array('status' => 500, 'message' => 'Ocurrio un error procesando la solicitud');
		}

		return array('status' => 200);
	}

	/**
	 * Prepare and send response
	 * */
	function json_output($statusHeader, $response){
		$ci =& get_instance();
		$ci->output->set_content_type('application/json');
		$ci->output->set_status_header($statusHeader);
		$ci->output->set_output(json_encode($response));
		$ci->output->_display();
		exit;
	}

	/**
	 * Get access token from header
	 * */
	function getBearerToken() {
	    $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }

	    // HEADER: Get the access token from the header
	    if (!empty($headers)) {
	        if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
	            return $matches[1];
	        }
	    }
	    return null;
	}
}