<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Asuncion');
		$this->load->model('User_model');
	}

	public function index()
	{
		//	CHECK HTTP HEADERS
		$checkHeaders = $this->rest->checkHeaders('GET');
		if ($checkHeaders['status'] != 200) {
			$this->rest->json_output($checkHeaders['status'],
				array('status' => $checkHeaders['status'],'message' => $checkHeaders['message'])
			);
		}

		//	CHECK TOKEN
		$checkToken = $this->rest->checkToken();
		if ($checkToken['status'] != 200) {
			$this->rest->json_output($checkToken['status'],
				array('status' => $checkToken['status'],'message' => $checkToken['message'])
			);
		}

		//	CHECK USER
		$checkUser = $this->rest->checkUser($checkToken['payload']);
		if ($checkUser['status'] != 200) {
			$this->rest->json_output($checkUser['status'],
				array('status' => $checkUser['status'],'message' => $checkUser['message'])
			);
		}

		//	FUNCTION
		$response = $this->User_model->getUsers();
	    $this->rest->json_output(200, $response);
	}

	public function getUser($id = null)
	{
		//	CHECK PARAM
		if(is_null($id)){
			$this->rest->json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}

		//	CHECK HTTP HEADERS
		$checkHeaders = $this->rest->checkHeaders('GET');
		if ($checkHeaders['status'] != 200) {
			$this->rest->json_output($checkHeaders['status'],
				array('status' => $checkHeaders['status'],'message' => $checkHeaders['message'])
			);
		}

		//	CHECK TOKEN
		$checkToken = $this->rest->checkToken();
		if ($checkToken['status'] != 200) {
			$this->rest->json_output($checkToken['status'],
				array('status' => $checkToken['status'],'message' => $checkToken['message'])
			);
		}

		//	CHECK USER
		$checkUser = $this->rest->checkUser($checkToken['payload']);
		if ($checkUser['status'] != 200) {
			$this->rest->json_output($checkUser['status'],
				array('status' => $checkUser['status'],'message' => $checkUser['message'])
			);
		}

		//	FUNCTION
		$response = $this->User_model->getUser($id);
	    $this->rest->json_output(200, $response);
	}

	public function postUser()
	{
		//	CHECK HTTP HEADERS
		$checkHeaders = $this->rest->checkHeaders('POST', 'application/json');
		if ($checkHeaders['status'] != 200) {
			$this->rest->json_output($checkHeaders['status'],
				array('status' => $checkHeaders['status'],'message' => $checkHeaders['message'])
			);
		}

		//	CHECK TOKEN
		$checkToken = $this->rest->checkToken();
		if ($checkToken['status'] != 200) {
			$this->rest->json_output($checkToken['status'],
				array('status' => $checkToken['status'],'message' => $checkToken['message'])
			);
		}

		//	CHECK USER
		$checkUser = $this->rest->checkUser($checkToken['payload']);
		if ($checkUser['status'] != 200) {
			$this->rest->json_output($checkUser['status'],
				array('status' => $checkUser['status'],'message' => $checkUser['message'])
			);
		}

		//	FUNCTION
		$user = json_decode(file_get_contents('php://input'),true);
		$result = $this->User_model->saveUser($user);
		if($result){
			$status = 201;
			$response = "Usuario agregado correctamente";
		}else{
			$status = 500;
			$response = "Ocurrio un error intentado agregar el usuario";
		}
	    $this->rest->json_output($status, $response);
	}

	public function patchUser($id = null)
	{
		//	CHECK PARAM
		if(is_null($id)){
			$this->rest->json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}
		
		//	CHECK HTTP HEADERS
		$checkHeaders = $this->rest->checkHeaders('PATCH', 'application/json');
		if ($checkHeaders['status'] != 200) {
			$this->rest->json_output($checkHeaders['status'],
				array('status' => $checkHeaders['status'],'message' => $checkHeaders['message'])
			);
		}

		//	CHECK TOKEN
		$checkToken = $this->rest->checkToken();
		if ($checkToken['status'] != 200) {
			$this->rest->json_output($checkToken['status'],
				array('status' => $checkToken['status'],'message' => $checkToken['message'])
			);
		}

		//	CHECK USER
		$checkUser = $this->rest->checkUser($checkToken['payload']);
		if ($checkUser['status'] != 200) {
			$this->rest->json_output($checkUser['status'],
				array('status' => $checkUser['status'],'message' => $checkUser['message'])
			);
		}

		//	FUNCTION
		$user = json_decode(file_get_contents('php://input'),true);
		$result = $this->User_model->updateUser($id, $user);
		if($result){
			$status = 201;
			$response = "Usuario actualizado correctamente";
		}else{
			$status = 500;
			$response = "Ocurrio un error intentado actualizar el usuario";
		}
	    $this->rest->json_output($status, $response);
	}

	public function putUser($id = null)
	{
		//	CHECK PARAM
		if(is_null($id)){
			$this->rest->json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}
		
		//	CHECK HTTP HEADERS
		$checkHeaders = $this->rest->checkHeaders('PUT', 'application/json');
		if ($checkHeaders['status'] != 200) {
			$this->rest->json_output($checkHeaders['status'],
				array('status' => $checkHeaders['status'],'message' => $checkHeaders['message'])
			);
		}

		//	CHECK TOKEN
		$checkToken = $this->rest->checkToken();
		if ($checkToken['status'] != 200) {
			$this->rest->json_output($checkToken['status'],
				array('status' => $checkToken['status'],'message' => $checkToken['message'])
			);
		}

		//	CHECK USER
		$checkUser = $this->rest->checkUser($checkToken['payload']);
		if ($checkUser['status'] != 200) {
			$this->rest->json_output($checkUser['status'],
				array('status' => $checkUser['status'],'message' => $checkUser['message'])
			);
		}

		//	FUNCTION
		$user = json_decode(file_get_contents('php://input'),true);
		$result = $this->User_model->replaceUser($id, $user);
		if($result){
			$status = 201;
			$response = "Usuario reemplazado correctamente";
		}else{
			$status = 500;
			$response = "Ocurrio un error intentado reemplazar el usuario";
		}
	    $this->rest->json_output($status, $response);
	}

	public function deleteUser($id = null)
	{
		//	CHECK PARAM
		if(is_null($id)){
			$this->rest->json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}
		
		//	CHECK HTTP HEADERS
		$checkHeaders = $this->rest->checkHeaders('DELETE', 'application/json');
		if ($checkHeaders['status'] != 200) {
			$this->rest->json_output($checkHeaders['status'],
				array('status' => $checkHeaders['status'],'message' => $checkHeaders['message'])
			);
		}

		//	CHECK TOKEN
		$checkToken = $this->rest->checkToken();
		if ($checkToken['status'] != 200) {
			$this->rest->json_output($checkToken['status'],
				array('status' => $checkToken['status'],'message' => $checkToken['message'])
			);
		}

		//	CHECK USER
		$checkUser = $this->rest->checkUser($checkToken['payload']);
		if ($checkUser['status'] != 200) {
			$this->rest->json_output($checkUser['status'],
				array('status' => $checkUser['status'],'message' => $checkUser['message'])
			);
		}

		//	FUNCTION
		$result = $this->User_model->deleteUser($id);
		if($result){
			$status = 201;
			$response = "Usuario eliminado correctamente";
		}else{
			$status = 500;
			$response = "Ocurrio un error intentado eliminar el usuario";
		}
	    $this->rest->json_output($status, $response);
	}
}
