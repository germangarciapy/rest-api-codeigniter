<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Controller {

	function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Asuncion');
		$this->load->model('Company_model');
	}

	public function index()
	{	
		//	CHECK HTTP HEADERS
		$checkHeaders = $this->rest->checkHeaders('GET');
		if ($checkHeaders['status'] != 200) {
			$this->rest->json_output($checkHeaders['status'],
				array('status' => $checkHeaders['status'],'message' => $checkHeaders['message'])
			);
		}

		//	CHECK TOKEN
		$checkToken = $this->rest->checkToken();
		if ($checkToken['status'] != 200) {
			$this->rest->json_output($checkToken['status'],
				array('status' => $checkToken['status'],'message' => $checkToken['message'])
			);
		}

		//	CHECK USER
		$checkUser = $this->rest->checkUser($checkToken['payload']);
		if ($checkUser['status'] != 200) {
			$this->rest->json_output($checkUser['status'],
				array('status' => $checkUser['status'],'message' => $checkUser['message'])
			);
		}

		//	FUNCTION
		$response = $this->Company_model->getCompanies();
	    $this->rest->json_output(200, $response);
	}

	public function getCompany($id = null)
	{
		//	CHECK PARAM
		if(is_null($id)){
			$this->rest->json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}
		
		//	CHECK HTTP HEADERS
		$checkHeaders = $this->rest->checkHeaders('GET');
		if ($checkHeaders['status'] != 200) {
			$this->rest->json_output($checkHeaders['status'],
				array('status' => $checkHeaders['status'],'message' => $checkHeaders['message'])
			);
		}

		//	CHECK TOKEN
		$checkToken = $this->rest->checkToken();
		if ($checkToken['status'] != 200) {
			$this->rest->json_output($checkToken['status'],
				array('status' => $checkToken['status'],'message' => $checkToken['message'])
			);
		}

		//	CHECK USER
		$checkUser = $this->rest->checkUser($checkToken['payload']);
		if ($checkUser['status'] != 200) {
			$this->rest->json_output($checkUser['status'],
				array('status' => $checkUser['status'],'message' => $checkUser['message'])
			);
		}

		//	FUNCTION
		$response = $this->Company_model->getCompany($id);
	    $this->rest->json_output(200, $response);
	}

	public function postCompany()
	{
		//	CHECK HTTP HEADERS
		$checkHeaders = $this->rest->checkHeaders('POST', 'application/json');
		if ($checkHeaders['status'] != 200) {
			$this->rest->json_output($checkHeaders['status'],
				array('status' => $checkHeaders['status'],'message' => $checkHeaders['message'])
			);
		}

		//	CHECK TOKEN
		$checkToken = $this->rest->checkToken();
		if ($checkToken['status'] != 200) {
			$this->rest->json_output($checkToken['status'],
				array('status' => $checkToken['status'],'message' => $checkToken['message'])
			);
		}

		//	CHECK USER
		$checkUser = $this->rest->checkUser($checkToken['payload']);
		if ($checkUser['status'] != 200) {
			$this->rest->json_output($checkUser['status'],
				array('status' => $checkUser['status'],'message' => $checkUser['message'])
			);
		}

		//	FUNCTION
		$company = json_decode(file_get_contents('php://input'),true);
		$result = $this->Company_model->saveCompany($company);
		if($result){
			$status = 201;
			$response = "Compañia agregada correctamente";
		}else{
			$status = 500;
			$response = "Ocurrio un error intentado agregar la compañia";
		}
	    $this->rest->json_output($status, $response);
	}

	public function patchCompany($id = null)
	{
		//	CHECK PARAM
		if(is_null($id)){
			$this->rest->json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}

		//	CHECK HTTP HEADERS
		$checkHeaders = $this->rest->checkHeaders('PATCH', 'application/json');
		if ($checkHeaders['status'] != 200) {
			$this->rest->json_output($checkHeaders['status'],
				array('status' => $checkHeaders['status'],'message' => $checkHeaders['message'])
			);
		}

		//	CHECK TOKEN
		$checkToken = $this->rest->checkToken();
		if ($checkToken['status'] != 200) {
			$this->rest->json_output($checkToken['status'],
				array('status' => $checkToken['status'],'message' => $checkToken['message'])
			);
		}

		//	CHECK USER
		$checkUser = $this->rest->checkUser($checkToken['payload']);
		if ($checkUser['status'] != 200) {
			$this->rest->json_output($checkUser['status'],
				array('status' => $checkUser['status'],'message' => $checkUser['message'])
			);
		}

		//	FUNCTION
		$company = json_decode(file_get_contents('php://input'),true);
		$result = $this->Company_model->updateCompany($id, $company);
		if($result){
			$status = 201;
			$response = "Compañia actualizada correctamente";
		}else{
			$status = 500;
			$response = "Ocurrio un error intentado actualizar la compañia";
		}
	    $this->rest->json_output($status, $response);
	}

	public function putCompany($id = null)
	{
		//	CHECK PARAM
		if(is_null($id)){
			$this->rest->json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}
		
		//	CHECK HTTP HEADERS
		$checkHeaders = $this->rest->checkHeaders('PUT', 'application/json');
		if ($checkHeaders['status'] != 200) {
			$this->rest->json_output($checkHeaders['status'],
				array('status' => $checkHeaders['status'],'message' => $checkHeaders['message'])
			);
		}

		//	CHECK TOKEN
		$checkToken = $this->rest->checkToken();
		if ($checkToken['status'] != 200) {
			$this->rest->json_output($checkToken['status'],
				array('status' => $checkToken['status'],'message' => $checkToken['message'])
			);
		}

		//	CHECK USER
		$checkUser = $this->rest->checkUser($checkToken['payload']);
		if ($checkUser['status'] != 200) {
			$this->rest->json_output($checkUser['status'],
				array('status' => $checkUser['status'],'message' => $checkUser['message'])
			);
		}

		//	FUNCTION
		$company = json_decode(file_get_contents('php://input'),true);
		$result = $this->Company_model->replaceCompany($id, $company);
		if($result){
			$status = 201;
			$response = "Compañia reemplazada correctamente";
		}else{
			$status = 500;
			$response = "Ocurrio un error intentado reemplazar la compañia";
		}
	    $this->rest->json_output($status, $response);
	}

	public function deleteCompany($id = null)
	{
		//	CHECK PARAM
		if(is_null($id)){
			$this->rest->json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}
		
		//	CHECK HTTP HEADERS
		$checkHeaders = $this->rest->checkHeaders('DELETE');
		if ($checkHeaders['status'] != 200) {
			$this->rest->json_output($checkHeaders['status'],
				array('status' => $checkHeaders['status'],'message' => $checkHeaders['message'])
			);
		}

		//	CHECK TOKEN
		$checkToken = $this->rest->checkToken();
		if ($checkToken['status'] != 200) {
			$this->rest->json_output($checkToken['status'],
				array('status' => $checkToken['status'],'message' => $checkToken['message'])
			);
		}

		//	CHECK USER
		$checkUser = $this->rest->checkUser($checkToken['payload']);
		if ($checkUser['status'] != 200) {
			$this->rest->json_output($checkUser['status'],
				array('status' => $checkUser['status'],'message' => $checkUser['message'])
			);
		}

		//	FUNCTION
		$result = $this->Company_model->deleteCompany($id);
		if($result){
			$status = 201;
			$response = "Compañia eliminada correctamente";
		}else{
			$status = 500;
			$response = "Ocurrio un error intentado eliminar la compañia";
		}
	    $this->rest->json_output($status, $response);
	}
}
