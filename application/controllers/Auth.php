<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'vendor/autoload.php'); 
use \Firebase\JWT\JWT;

class Auth extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Auth_model');
		date_default_timezone_set('America/Asuncion');
	}

	/*
     * Validar datos del login
     */
	public function login(){
		//	CHECK HTTP HEADERS
		$checkHeaders = $this->rest->checkHeaders('POST', 'application/json');
		if ($checkHeaders['status'] != 200) {
			$this->rest->json_output($checkHeaders['status'],
				array('status' => $checkHeaders['status'],'message' => $checkHeaders['message'])
			);
		}

		//	CHECK PARAMS
		$params = json_decode(file_get_contents('php://input'),true);
		if(!isset($params['mail']) OR !isset($params['password'])){
			$this->rest->json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}

		$result = $this->Auth_model->login($params['mail'], $params['password']);
		switch ($result['message']) {
			case 'invalid_user':
				$this->rest->json_output(401, 'Usuario no encontrado.');
				break;
			case 'invalid_password':
				$this->rest->json_output(401, 'Contraseña incorrecta.');
				break;
			case 'success':
				$payload = [
					"iat" => time(),
					"exp" => time() + (3600),
					"userId" => $result['userId']
				];
				try {
					$token = JWT::encode($payload, SECRET_KEY);	
				} catch (Exception $e) {
					$this->rest->json_output(500, 'Ocurrio un error resolviendo la solicitud.');
				}

				$response = array('status' => 200, 'message' => 'Usuario registrado exitosamente', 'token' => $token);
				$this->rest->json_output(200, $response);
				break;
			default:
				$this->rest->json_output(500, 'Ocurrio un error resolviendo la solicitud.');
				break;
		}
	}

}
